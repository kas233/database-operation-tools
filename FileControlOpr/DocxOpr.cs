﻿using System;
using System.Data;
using System.Drawing;
using Xceed.Document.NET;
using Xceed.Words.NET;

namespace FileControlOpr
{
    public static class DocXOpr
    {
        public static DocX Load(string path)
        {
            var document = DocX.Load(path);

            return document;
        }

        public static DocX Create(string path)
        {
            var document = DocX.Create(path);

            return document;
        }

        public static Table InsertTableByDataTable(DocX doc, DataTable dt,
            Color headColor = new Color(), AutoFit autoFit = AutoFit.Fixed, bool headBoldFlag = false, int contentSize = 0)
        {
            var tb = doc.AddTable(dt.Rows.Count + 1, dt.Columns.Count);

            for (int i = 0; i < tb.Rows.Count; i++)
            {
                if (i == 0)
                {
                    for (int j = 0; j < tb.ColumnCount; j++)
                    {
                        if (!headColor.IsEmpty)
                        {
                            tb.Rows[i].Cells[j].FillColor = headColor;
                        }
                        tb.Rows[i].Cells[j].Paragraphs[0].Append(dt.Columns[j].ToString());
                        if (headBoldFlag)
                        {
                            tb.Rows[i].Cells[j].Paragraphs[0].Bold();
                        }
                        if (contentSize > 0)
                        {
                            tb.Rows[i].Cells[j].Paragraphs[0].FontSize(contentSize);
                        }
                    }
                }
                else
                {
                    for (int j = 0; j < tb.ColumnCount; j++)
                    {
                        tb.Rows[i].Cells[j].Paragraphs[0].Append(dt.Rows[i - 1][j].ToString());
                        if (contentSize > 0)
                        {
                            tb.Rows[i].Cells[j].Paragraphs[0].FontSize(contentSize);
                        }
                    }
                }
            }
            tb.AutoFit = autoFit;
            doc.InsertTable(tb);

            return tb;
        }

        public static Paragraph InsertParagraph(DocX doc, string content, int size = 0, bool boldFlag = false)
        {
            var p = doc.InsertParagraph(content);

            if (boldFlag)
            {
                p.Bold();
            }
            if (size > 0)
            {
                p.FontSize(size);
            }

            return p;
        }

    }
}
