﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SQLToolsCore
{
    public partial class SetCommonBtn : Form
    {
        public List<ButtonInfo> btnInfoLst;
        private readonly bool devFlag = true;

        public SetCommonBtn(List<ButtonInfo> buttonInfoLst)
        {
            InitializeComponent();
            btnInfoLst = buttonInfoLst;
            dataGridView1.DataSource = TransBtnLst(buttonInfoLst);
            dataGridView1.Columns[1].ReadOnly = true;
            if (!devFlag)
            {
                dataGridView1.Columns[3].Visible = false;
            }
        }

        private DataTable TransBtnLst(List<ButtonInfo> buttonInfoLst)
        {
            var dt = new DataTable();

            dt.Columns.AddRange(new string[] { "是否常用", "按钮名称", "说明", "对应方法名" }.Select(x => new DataColumn(x)).ToArray());
            dt.Columns[0].DataType = typeof(bool);

            foreach (var item in buttonInfoLst)
            {
                if (!devFlag && !item.BaseFlag)
                    continue;

                var newDr = dt.NewRow();
                newDr[0] = item.CommonFlag;
                newDr[1] = item.ButtonName;
                newDr[2] = item.TipStr;
                newDr[3] = item.FuncName;

                dt.Rows.Add(newDr);
            }

            return dt;
        }

        private void UpdateBtnLst()
        {
            foreach (DataGridViewRow item in dataGridView1.Rows)
            {
                var btnInfo = btnInfoLst.FirstOrDefault(x => x.ButtonName == item.Cells[1].Value.ToString());
                btnInfo.CommonFlag = bool.Parse(item.Cells[0].Value.ToString());
                btnInfo.TipStr = item.Cells[2].Value.ToString();
                btnInfo.FuncName = item.Cells[3].Value.ToString();
            }
        }

        private void SaveBtnLst(object sender, EventArgs e)
        {
            UpdateBtnLst();

            if (btnInfoLst.Count(x => x.CommonFlag == true) > 5)
            {
                MessageBox.Show("常用按钮设置不能超过5个！");
            }
            else
            {
                this.DialogResult = DialogResult.OK;
            }
        }
    }
}
