﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataBaseFactoryLib;

namespace SQLToolsCore
{
    public partial class Executer : Form
    {
        private Login mainForm;
        private string selFilePath;
        private BaseDataBaseOpr db = DBHelper.GetInstanse();

        public Executer(List<string> DBNameLst, Login form1)
        {
            InitializeComponent();
            comboBox1.DataSource = DBNameLst;
            mainForm = form1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new();
            dialog.Multiselect = true;//该值确定是否可以选择多个文件
            dialog.Title = "请选择文件";
            dialog.Filter = "SQL脚本(*.sql)|*.sql";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                selFilePath = dialog.FileName.Replace(dialog.SafeFileName, "");
                checkedListBox1.DataSource = dialog.SafeFileNames;
                textBox1.Text = dialog.FileName.Replace(dialog.SafeFileName, "");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (checkedListBox1.CheckedItems.Count == checkedListBox1.Items.Count)
            {
                for (int i = 0; i < checkedListBox1.Items.Count; i++)
                {
                    checkedListBox1.SetItemChecked(i, false);
                }
            }
            else
            {
                for (int i = 0; i < checkedListBox1.Items.Count; i++)
                {
                    checkedListBox1.SetItemChecked(i, true);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (checkedListBox1.CheckedItems.Count <= 0)
            {
                MessageBox.Show("请选择要执行的脚本.");
                return;
            }

            DialogResult dr = MessageBox.Show("确认执行选择脚本？", "确认", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (dr == DialogResult.OK)
            {
                try
                {
                    string erroMsg = string.Empty;
                    for (int i = 0; i < checkedListBox1.CheckedItems.Count; i++)
                    {
                        int value = (i + 1) * 10000 / checkedListBox1.CheckedItems.Count;

                        StreamReader sr = new(selFilePath + checkedListBox1.CheckedItems[i], Encoding.Default);
                        string line;
                        string tempSql = string.Empty, tempMsg = string.Empty;

                        while ((line = sr.ReadLine()) != null)
                        {
                            if (line.ToUpper().Contains("GO"))
                            {
                                tempMsg += ExcuteSQLStr(tempSql);
                                tempSql = string.Empty;
                            }
                            else
                            {
                                tempSql += line + "\r";
                            }
                        }

                        sr.Close();

                        if (!string.IsNullOrWhiteSpace(tempSql))
                        {
                            tempMsg = ExcuteSQLStr(tempSql);
                        }
                        if (!string.IsNullOrWhiteSpace(tempMsg))
                        {
                            erroMsg += "\"" + checkedListBox1.CheckedItems[i] + "\"脚本错误," + tempMsg + "\r";
                        }

                        progressBar1.Value = value;
                    }
                    MessageBox.Show("执行完成.\r" + erroMsg);
                    progressBar1.Value = 0;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private string ExcuteSQLStr(string sqlStr)
        {
            string tempMsg = string.Empty;
            try
            {
                db.ExecuteNonQuery(sqlStr);
            }
            catch (Exception ex)
            {
                tempMsg = ex.Message;
            }
            return tempMsg;
        }

        private void this_FormClosed(object sender, FormClosedEventArgs e)
        {
            db.Connection.Dispose();
            mainForm.Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            db.SetDataBaseName(comboBox1.Text);
        }
    }
}
