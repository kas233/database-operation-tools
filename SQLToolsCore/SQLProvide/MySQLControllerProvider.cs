﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataBaseFactoryLib;

namespace SQLToolsCore.SQLProvide
{
    public class MySQLControllerProvider : IControllerSQLProvider
    {
        internal BaseDataBaseOpr db = DBHelper.GetInstanse();

        public DataTable RetrieveDataStrucName(string conStr, bool tableFlag, bool procedureFlag, bool funcFlag)
        {
            string baseSQL = @"select table_name name,table_name stName from information_schema.TABLES 
            where TABLE_SCHEMA='{0}' {1}";

            string sqlCon = string.Empty;
            if (!string.IsNullOrWhiteSpace(conStr))
            {
                sqlCon += "AND replace(table_name,'_','') LIKE '%" + conStr.Replace("_", "") + "%'";
            }

            return db.QueryDataTable(string.Format(baseSQL, db.DBName, sqlCon));
        }

        public DataTable GetTableStructue(string tableName)
        {
            string baseSQL = $"show full columns from {tableName};";
            var dt = db.QueryDataTable(baseSQL);
            return ConvertToCommonTableStructue(dt);
        }

        private static DataTable ConvertToCommonTableStructue(DataTable dt)
        {
            var rtnDt = new DataTable();

            rtnDt.Columns
                .AddRange(new string[] { "序号", "字段名", "数据类型", "空否", "默认值", "别名", "备注", "主键" }
                .Select(x => new DataColumn(x)).ToArray());

            int i = 0;
            foreach (DataRow dr in dt.Rows)
            {
                i++;
                var newDr = rtnDt.NewRow();
                newDr.ItemArray = new object[] { i.ToString(), dr["Field"], dr["Type"],
                    dr["Null"].ToString()[0..1], dr["Default"].ToString(),"", "",  dr["Key"].ToString() == "PRI"? "Y":"N"};

                string oroRemark = dr["Comment"].ToString();
                newDr["别名"] = string.IsNullOrWhiteSpace(oroRemark) || !oroRemark.Contains("(") ? oroRemark : oroRemark[0..oroRemark.IndexOf("(")];
                newDr["备注"] = string.IsNullOrWhiteSpace(oroRemark) || !oroRemark.Contains("(") ? "" : oroRemark[(oroRemark.IndexOf("(") + 1)..^1];

                rtnDt.Rows.Add(newDr);
            }

            return rtnDt;
        }
    }
}
