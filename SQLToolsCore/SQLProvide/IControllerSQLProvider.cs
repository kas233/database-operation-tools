﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLToolsCore.SQLProvide
{
    public interface IControllerSQLProvider
    {
        /// <summary>
        /// 查询数据结构名称
        /// </summary>
        /// <param name="conStr">数据结构名称</param>
        /// <param name="tableFlag">是否为表</param>
        /// <param name="procedureFlag">是否为存储过程</param>
        /// <param name="funcFlag">是否为函数</param>
        /// <returns></returns>
        public DataTable RetrieveDataStrucName(string conStr, bool tableFlag, bool procedureFlag, bool funcFlag);

        /// <summary>
        /// 获取表结构
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <returns></returns>
        public DataTable GetTableStructue(string tableName);
    }
}
