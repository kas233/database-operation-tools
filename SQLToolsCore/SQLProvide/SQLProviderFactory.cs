﻿using DataBaseFactoryLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLToolsCore.SQLProvide
{
    public class SQLProviderFactory
    {
        private readonly DataBaseTypeEnum dbType;

        public SQLProviderFactory(DataBaseTypeEnum dataBaseType)
        {
            dbType = dataBaseType;
        }

        public IControllerSQLProvider CreateSQLProvider()
        {
            switch (dbType)
            {
                case DataBaseTypeEnum.MYSQL:
                    return new MySQLControllerProvider();
                case DataBaseTypeEnum.MSSQL:
                    return new MSSQLControllerProvider();
                case DataBaseTypeEnum.Oracle:
                    return new OrcleControllerProvider();
                default:
                    return null;
            }
        }
    }
}
