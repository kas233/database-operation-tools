﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataBaseFactoryLib;

namespace SQLToolsCore
{
    #region 表结构类
    public class TableInfo
    {
        public TableInfo()
        {
            TableName = string.Empty;
            LastTableName = string.Empty;
            DataStrucs = new List<DataStruc>();
        }

        public string TableName { get; set; }

        public string LowerTableName { get; set; }

        public string LastTableName { get; set; }

        public string FLLastTableName { get; set; }

        public List<DataStruc> DataStrucs { get; set; }
    }

    public class DataStruc
    {
        public DataStruc()
        {
            FiledName = string.Empty;
            DataType = string.Empty;
            IsNull = string.Empty;
            DefaultValue = string.Empty;
            Remark = string.Empty;
            FiledName = string.Empty;
        }

        [Column(Name = "字段名")]
        public string FiledName { get; set; }

        [Column(Name = "数据类型")]
        public string DataType { get; set; }

        [Column(Name = "空否")]
        public string IsNull { get; set; }

        [Column(Name = "默认值")]
        public string DefaultValue { get; set; }

        [Column(Name = "别名")]
        public string AliasName { get; set; }

        [Column(Name = "备注")]
        public string Remark { get; set; }

        [Column(Name = "主键")]
        public string PrimaryKey { get; set; }

        public int Index { get; set; }

        public string FiledNameMember { get; set; }

        public string FiledNamePropertie { get; set; }

        public string CodeDataType { get; set; }

        public string CodeDeflautVal { get; set; }

        public string PrimaryKeySign { get; set; }
    }
    #endregion

    #region 按钮记录对象
    [Serializable]
    public class ButtonInfo
    {
        /// <summary>
        /// 按钮名称
        /// </summary>
        public string ButtonName { get; set; }
        /// <summary>
        /// 常用标志
        /// </summary>
        public bool CommonFlag { get; set; }
        /// <summary>
        /// 基础按钮标志
        /// </summary>
        public bool BaseFlag { get; set; }
        /// <summary>
        /// 提示说明
        /// </summary>
        public string TipStr { get; set; }
        /// <summary>
        /// 对应类中函数名(用于反射，加载事件)
        /// </summary>
        public string FuncName { get; set; }
    }
    #endregion

    #region 连接对象
    [Serializable]
    public class ConnectionHistory
    {
        public int DataBaseType { get; set; }

        public string Server { get; set; }

        public int Port { get; set; }

        public bool ConnType { get; set; }

        public string Uid { get; set; }

        public string Password { get; set; }

        public ConnectionHistory Clone()
        {
            return this.MemberwiseClone() as ConnectionHistory;
        }
    }
    #endregion
}
