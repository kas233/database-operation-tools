﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SQLToolsCore
{
    public class ConnectHistoryOpr
    {
        private static readonly string _filePath = AppDomain.CurrentDomain.BaseDirectory + @"resrc\ch.dat";

        /// <summary>
        /// 获取连接历史记录
        /// </summary>
        /// <returns></returns>
        public static List<ConnectionHistory> GetConnectionHistory()
        {
            return File.Exists(_filePath) ? CommonTools.LoadContentWithDeserialize<List<ConnectionHistory>>(_filePath) : new List<ConnectionHistory>();
        }

        public static void SaveConnectionHistory(List<ConnectionHistory> connections)
        {
            CommonTools.SaveContentWithSerialize(_filePath, connections);
        }

    }

}
