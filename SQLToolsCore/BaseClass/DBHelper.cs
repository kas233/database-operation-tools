﻿using DataBaseFactoryLib;
using System;

namespace SQLToolsCore
{
    internal class DBHelper
    {

        public static BaseDataBaseOpr dbOpr;

        public static BaseDataBaseOpr GetInstanse(DataBaseFactory dataBaseFactory = null)
        {

            if (dbOpr == null)
            {
                if (dataBaseFactory == null)
                {
                    throw new Exception("数据库工厂缺失");
                }
                dbOpr = dataBaseFactory.CreateDataBaseOpr();
            }

            return dbOpr;
        }
    }
}