﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLToolsCore
{
    public class BtnInfoOpr
    {
        private static readonly string _buttonInfoPath = AppDomain.CurrentDomain.BaseDirectory + @"resrc\bt.dat";

        public static void SaveBtnInfoLst(List<ButtonInfo> btnInfoLst)
        {
            CommonTools.SaveContentWithSerialize(_buttonInfoPath, btnInfoLst);
        }

        public static List<ButtonInfo> LoadBtnInfoLst()
        {
            return CommonTools.LoadContentWithDeserialize<List<ButtonInfo>>(_buttonInfoPath);
        }
    }
}
