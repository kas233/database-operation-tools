﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SQLToolsCore
{
    public partial class StructureShower : Form
    {
        public StructureShower(DataTable dt)
        {
            InitializeComponent();
            this.Text += dt.TableName;
            dataGridView1.DataSource = dt;
        }

        private void StructureShower_Load(object sender, EventArgs e)
        {
            var oriWidth = dataGridView1.Width;
            var oriHeight = dataGridView1.Height;

            var newWidth = 0;
            foreach (DataGridViewColumn item in dataGridView1.Columns)
            {
                newWidth += item.Width;
            }
            dataGridView1.Width = newWidth;

            if ((dataGridView1.Rows.Count + 1) * dataGridView1.Rows[0].Height > 600)
            {
                dataGridView1.Width += 20;
                dataGridView1.ScrollBars = ScrollBars.Vertical;
                dataGridView1.Height = 450;
            }
            else
            {
                dataGridView1.Height = (dataGridView1.Rows.Count + 1) * dataGridView1.Rows[0].Height;
            }

            this.Width += dataGridView1.Width - oriWidth;
            this.Height += dataGridView1.Height - oriHeight;
        }
    }
}
