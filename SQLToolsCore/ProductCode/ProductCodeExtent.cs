﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SQLToolsCore.ProductCode
{
    public static class ProductCodeExtent
    {
        /// <summary>
        /// 去除前缀(用于去除表名的前缀，创建对应的类名等)
        /// </summary>
        /// <param name="orgStr">原字符串</param>
        /// <param name="splitChar">分隔符</param>
        /// <returns></returns>
        public static string RemovePrefix(this string orgStr, string splitChar = "_")
        {
            if (string.IsNullOrWhiteSpace(orgStr))
            {
                return orgStr;
            }

            var baseStr = orgStr.Split(splitChar).ToList().Last();
            return baseStr.Substring(0, 1).ToUpper() + baseStr[1..];
        }

        /// <summary>
        /// 根据命名方法生成对应字符串(用于生成对象属性)
        /// </summary>
        /// <param name="orgStr">原字符创</param>
        /// <param name="rule">规则(保持、驼峰、帕斯卡)</param>
        /// <param name="splitChar">分隔符</param>
        /// <returns></returns>
        public static string SetNamingRule(this string orgStr, NamingRule rule, string splitChar = "_")
        {
            FieldInfo fi = typeof(NamingRule).GetField(rule.ToString());
            var attribute = fi.GetCustomAttribute<NamingRuleFuncAttribute>(false);
            if (attribute == null)
            {
                return orgStr;
            }

            var methodArray = typeof(ProductCodeExtent).GetMethods(BindingFlags.NonPublic | BindingFlags.Static);
            var func = methodArray.FirstOrDefault(x => x.Name == attribute.FuncName);

            return func.Invoke(null, new object[] { orgStr, splitChar }) as string;
        }

        private static string SetCamelCase(string orgStr, string splitChar)
        {
            if (!string.IsNullOrWhiteSpace(orgStr))
            {
                var partsOfOrgStr = orgStr.Split(splitChar).ToList();
                var rtnVal = string.Empty;
                foreach (var item in partsOfOrgStr)
                {
                    rtnVal += item.Substring(0, 1).ToUpper() + item[1..];
                }
                return rtnVal;
            }
            else
            {
                return orgStr;
            }
        }

        private static string SetPascal(string orgStr, string splitChar)
        {
            if (!string.IsNullOrWhiteSpace(orgStr))
            {
                var partsOfOrgStr = orgStr.Split(splitChar).ToList();
                var rtnVal = string.Empty;
                foreach (var item in partsOfOrgStr)
                {
                    if (partsOfOrgStr.IndexOf(item) == 0)
                        rtnVal += item.Substring(0, 1).ToLower() + item[1..];
                    else
                        rtnVal += item.Substring(0, 1).ToUpper() + item[1..];
                }
                return rtnVal;
            }
            else
            {
                return orgStr;
            }
        }
    }

    public enum NamingRule
    {
        Original = 1,

        [NamingRuleFunc("SetCamelCase")]
        CamelCase = 2,

        [NamingRuleFunc("SetPascal")]
        Pascal = 3
    }

    [AttributeUsage(AttributeTargets.Field)]
    public class NamingRuleFuncAttribute : Attribute
    {
        public NamingRuleFuncAttribute(string funcName)
        {
            FuncName = funcName;
        }

        public string FuncName { get; set; }
    }

}
