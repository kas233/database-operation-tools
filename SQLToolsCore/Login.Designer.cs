﻿
namespace SQLToolsCore
{
    partial class Login
    {
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.uid = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.pwd = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.connType = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.server = new System.Windows.Forms.ComboBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dataBaseType = new System.Windows.Forms.ComboBox();
            this.PortLabel = new System.Windows.Forms.Label();
            this.PortValue = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(43, 79);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "服务器名称:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(73, 177);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "登录名:";
            // 
            // uid
            // 
            this.uid.Location = new System.Drawing.Point(156, 174);
            this.uid.Name = "uid";
            this.uid.Size = new System.Drawing.Size(247, 27);
            this.uid.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(88, 226);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 20);
            this.label3.TabIndex = 5;
            this.label3.Text = "密码:";
            // 
            // pwd
            // 
            this.pwd.Location = new System.Drawing.Point(156, 222);
            this.pwd.Name = "pwd";
            this.pwd.PasswordChar = '*';
            this.pwd.Size = new System.Drawing.Size(247, 27);
            this.pwd.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(58, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 20);
            this.label4.TabIndex = 7;
            this.label4.Text = "身份验证:";
            // 
            // connType
            // 
            this.connType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.connType.FormattingEnabled = true;
            this.connType.Items.AddRange(new object[] {
            "SQL Server 身份验证",
            "Windows 身份验证"});
            this.connType.Location = new System.Drawing.Point(156, 125);
            this.connType.Name = "connType";
            this.connType.Size = new System.Drawing.Size(247, 28);
            this.connType.TabIndex = 8;
            this.connType.SelectedIndexChanged += new System.EventHandler(this.MSSQLConnectionTypeChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(73, 276);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(124, 35);
            this.button1.TabIndex = 9;
            this.button1.Text = "执行";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.ShowExecuter);
            // 
            // server
            // 
            this.server.FormattingEnabled = true;
            this.server.Location = new System.Drawing.Point(156, 76);
            this.server.Name = "server";
            this.server.Size = new System.Drawing.Size(247, 28);
            this.server.TabIndex = 10;
            this.server.SelectedIndexChanged += new System.EventHandler(this.HistoryServerRecordChanged);
            this.server.TextChanged += new System.EventHandler(this.ReInputServerInfo);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(269, 276);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(124, 35);
            this.button2.TabIndex = 11;
            this.button2.Text = "操作";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.ShowController);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft YaHei UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label5.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.label5.Location = new System.Drawing.Point(414, 318);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 20);
            this.label5.TabIndex = 12;
            this.label5.Text = "V0.9.1";
            this.label5.Click += new System.EventHandler(this.ShowVersionInfo);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(43, 30);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 20);
            this.label6.TabIndex = 13;
            this.label6.Text = "数据库类型:";
            // 
            // dataBaseType
            // 
            this.dataBaseType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dataBaseType.FormattingEnabled = true;
            this.dataBaseType.Items.AddRange(new object[] {
            "SQL Server",
            "MySql(8.00+)"});
            this.dataBaseType.Location = new System.Drawing.Point(156, 27);
            this.dataBaseType.Name = "dataBaseType";
            this.dataBaseType.Size = new System.Drawing.Size(247, 28);
            this.dataBaseType.TabIndex = 14;
            this.dataBaseType.SelectedIndexChanged += new System.EventHandler(this.DataBaseTypeChanged);
            // 
            // PortLabel
            // 
            this.PortLabel.AutoSize = true;
            this.PortLabel.Location = new System.Drawing.Point(87, 128);
            this.PortLabel.Name = "PortLabel";
            this.PortLabel.Size = new System.Drawing.Size(43, 20);
            this.PortLabel.TabIndex = 15;
            this.PortLabel.Text = "端口:";
            // 
            // PortValue
            // 
            this.PortValue.Location = new System.Drawing.Point(156, 125);
            this.PortValue.Name = "PortValue";
            this.PortValue.Size = new System.Drawing.Size(247, 27);
            this.PortValue.TabIndex = 16;
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(466, 342);
            this.Controls.Add(this.PortValue);
            this.Controls.Add(this.PortLabel);
            this.Controls.Add(this.dataBaseType);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.server);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.connType);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pwd);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.uid);
            this.Controls.Add(this.label1);
            this.Icon = global::SQLToolsCore.Properties.Resources.bitbug_favicon;
            this.MaximizeBox = false;
            this.Name = "Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "连接";
            this.ResumeLayout(false);
            this.PerformLayout();
            this.Shown += Login_Shown;

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox uid;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox pwd;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox connType;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox server;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox dataBaseType;
        private System.Windows.Forms.Label PortLabel;
        private System.Windows.Forms.TextBox PortValue;
    }
}

