# 数据库操作工具

#### 介绍
提供根据模版生成代码、查看数据结构、导出数据结构、导入数据结构、快速增加单条数据等等数据库相关功能。
解决开发过程中重复性或繁杂的工作过多的问题，一劳永逸。
目前已支持SQLServer、MySQL。

#### 软件框架及引用插件


![框架](https://img.shields.io/badge/.NET_Core-5.0.0-7fb80e "框架") ![NPOI](https://img.shields.io/badge/DotNETCore.NPOI-1.2.3-blue "NPOI")


#### 使用说明

打开工具，输入数据库连接，开始工作吧！Do it!

