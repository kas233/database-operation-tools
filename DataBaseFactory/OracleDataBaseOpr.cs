﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.OracleClient;
using Oracle.ManagedDataAccess.Client;
using System.Data.Common;

namespace DataBaseFactoryLib
{
    class OracleDataBaseOpr : BaseDataBaseOpr
    {
        protected override void SetDataBaseType()
        {
            dataBaseType = DataBaseTypeEnum.Oracle;
        }

        protected override void InstanceDBConn()
        {
            conn = new OracleConnection();
        }

        protected override DbDataAdapter InstanceDbDataAdapter()
        {
            return new OracleDataAdapter();
        }

        protected override string GetConnStr()
        {
            return $"Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST={server})(PORT={port}))(CONNECT_DATA=(SERVICE_NAME=***)));Persist Security Info=True;User ID={uid};Password={pwd};";
        }
    }
}
