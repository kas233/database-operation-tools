﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataBaseFactoryLib
{
    public enum DataBaseTypeEnum
    {
        [DBOprClass("DataBaseFactoryLib.MSSQLDataBaseOpr")]
        MSSQL,
        [DBOprClass("DataBaseFactoryLib.MySQLDataBaseOpr")]
        MYSQL,
        [DBOprClass("DataBaseFactoryLib.OracleDataBaseOpr")]
        Oracle,
        ACCESS
    }

    [AttributeUsage(AttributeTargets.Field)]
    public class DBOprClassAttribute : Attribute
    {
        public DBOprClassAttribute(string className)
        {
            ClassName = className;
        }

        public string ClassName { get; set; }
    }
}
