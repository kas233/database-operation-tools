﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;

namespace DataBaseFactoryLib
{
    public class MySQLDataBaseOpr : BaseDataBaseOpr
    {
        protected override void SetDataBaseType()
        {
            dataBaseType = DataBaseTypeEnum.MYSQL;
        }

        protected override void InstanceDBConn()
        {
            conn = new MySqlConnection();
        }

        protected override DbDataAdapter InstanceDbDataAdapter()
        {
            return new MySqlDataAdapter();
        }

        protected override string GetConnStr()
        {
            return $"server={server};port={port};user={uid};password={pwd};database={DBName};Charset=utf8;";
        }
    }
}
