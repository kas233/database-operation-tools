﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataBaseFactoryLib
{
    #region 自定义特性类
    [AttributeUsage(AttributeTargets.Class)]
    public class TableAttribute : Attribute
    {
        public TableAttribute()
        {
            Name = string.Empty;
            IsDataExtend = false;
            IsNotEditExtend = false;
        }

        public bool IsDataExtend { get; set; }
        public bool IsNotEditExtend { get; set; }
        public string Name { get; set; }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class ColumnAttribute : Attribute
    {
        public ColumnAttribute()
        {
            Name = string.Empty;
            Ignore = false;
            IsInsert = true;
            IsNull = true;
            IsUnique = true;
            IsUpdate = true;
        }

        public string Name { get; set; }
        public bool Ignore { get; set; }
        public bool IsInsert { get; set; }
        public bool IsNull { get; set; }
        public bool IsUnique { get; set; }
        public bool IsUpdate { get; set; }
    }

    public class IdAttribute : ColumnAttribute
    {

    }
    #endregion
}
