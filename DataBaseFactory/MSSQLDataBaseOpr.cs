﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

namespace DataBaseFactoryLib
{
    public class MSSQLDataBaseOpr : BaseDataBaseOpr
    {
        protected override void SetDataBaseType()
        {
            dataBaseType = DataBaseTypeEnum.MSSQL;
        }

        protected override void InstanceDBConn()
        {
            conn = new SqlConnection();
        }

        protected override DbDataAdapter InstanceDbDataAdapter()
        {
            return new SqlDataAdapter();
        }

        protected override string GetConnStr()
        {
            server = port == 0 ? server : server + ',' + port.ToString();
            if (string.IsNullOrWhiteSpace(uid) && string.IsNullOrWhiteSpace(pwd))
            {
                return $"Data Source={server};Initial Catalog={DBName};Integrated Security=True";
            }
            else
            {
                return $"server={server};database={DBName};Uid={uid};pwd={pwd};Enlist=true";
            }
        }
    }
}
